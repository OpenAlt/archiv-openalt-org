'use strict';

const archiveHostname = 'archiv.openalt.org';
const nextCloudHostname = 'cloud.openalt.org';

function redirectTo(url) {
  window.location = url;
}

function redirectToInArchive(pathInArchive, pathname) {
  redirectTo(`https://${archiveHostname}${pathInArchive}${pathname}${window.location.search}${window.location.hash}`);
}

function redirectToFileInNextCloud(shareId) {
  redirectTo(`https://${nextCloudHostname}/s/${shareId}/download?path=%2F&files=${window.location.pathname.split('/').pop()}`);
}

function tryRedirectPhpToHtml() {
  const pathWithoutExtensionRegex = /^(.*\/[a-zA-Z0-9_-]+)\.php(\?.*)?$/i;
  const match = pathWithoutExtensionRegex.exec(window.location.pathname);
  if (match) {
    redirectToInArchive(match[1], '.html');
  }
}

if (window.location.hostname === archiveHostname) {
  if (/^\/openalt-cz\/2013\/.*$/i.test(window.location.pathname)) {
    redirectToInArchive('/linuxalt-cz', window.location.pathname.slice('/openalt-cz'.length));
  }

  if (/^\/openalt-cz\/2014\/data(\/)?$/i.test(window.location.pathname)) {
    redirectTo(`https://${nextCloudHostname}/s/pfmNSkAKMKSswRQ`);
  }

  if (/^\/openalt-cz\/2014\/data\/.+$/i.test(window.location.pathname)) {
    redirectToFileInNextCloud('pfmNSkAKMKSswRQ');
  }

  if (/^\/openalt-cz\/2015(\/|(\/index\.html?|\/index\.php))?$/i.test(window.location.pathname)) {
    redirectToInArchive('/openalt-cz', '/2015/cs/home.html');
  }

  if (/^\/openalt-cz\/2015\/data(\/)?$/i.test(window.location.pathname)) {
    redirectTo(`https://${nextCloudHostname}/s/i9yRAfPoGyXKgYf`);
  }

  if (/^\/openalt-cz\/2015\/data\/.+$/i.test(window.location.pathname)) {
    redirectToFileInNextCloud('i9yRAfPoGyXKgYf');
  }

  if (/^\/openalt-cz\/2016\/data(\/)?$/i.test(window.location.pathname)) {
    redirectTo(`https://${nextCloudHostname}/s/H49YTmMorWqZpMy`);
  }

  if (/^\/openalt-cz\/2016\/data\/.+$/i.test(window.location.pathname)) {
    redirectToFileInNextCloud('H49YTmMorWqZpMy');
  }

  if (/^\/openalt-cz\/2017\/slides(\/)?$/i.test(window.location.pathname)) {
    redirectTo(`https://${nextCloudHostname}/s/oeW9aRd42zYY5q9`);
  }

  if (/^\/openalt-cz\/2017\/slides\/.+$/i.test(window.location.pathname)) {
    redirectToFileInNextCloud('oeW9aRd42zYY5q9');
  }

  if (/^\/openalt-cz\/2018\/avatars(\/)?$/i.test(window.location.pathname)) {
    redirectTo(`https://${nextCloudHostname}/s/MbtwAKNLjdgdtLa`);
  }

  if (/^\/openalt-cz\/2018\/avatars\/.+$/i.test(window.location.pathname)) {
    redirectToFileInNextCloud('MbtwAKNLjdgdtLa');
  }

  if (/^\/openalt-cz\/2019\/slides(\/)?$/i.test(window.location.pathname)) {
    redirectTo(`https://${nextCloudHostname}/s/RdSBMxgdzPN9yQf`);
  }

  if (/^\/openalt-cz\/2019\/slides\/.+$/i.test(window.location.pathname)) {
    redirectToFileInNextCloud('RdSBMxgdzPN9yQf');
  }

  if (/^\/openalt-cz\/2020\/slides(\/)?$/i.test(window.location.pathname)) {
    redirectTo(`https://${nextCloudHostname}/s/t42gG6iiHgFoHkr`);
  }

  if (/^\/openalt-cz\/2020\/slides\/.+$/i.test(window.location.pathname)) {
    redirectToFileInNextCloud('t42gG6iiHgFoHkr');
  }

  if (/^\/openalt-cz\/2020\/zaznamy\/.+$/i.test(window.location.pathname)) {
    redirectToFileInNextCloud('dmPKZaasTTcdmcr');
  }

  if (/^\/openalt-cz\/2021\/slides(\/)?$/i.test(window.location.pathname)) {
    redirectTo(`https://${nextCloudHostname}/s/S6ZKYdSZpLBJpca`);
  }

  if (/^\/openalt-cz\/2021\/slides\/.+$/i.test(window.location.pathname)) {
    redirectToFileInNextCloud('S6ZKYdSZpLBJpca');
  }

  if (/^\/openalt-cz\/2021\/data\/.+$/i.test(window.location.pathname)) {
    redirectToFileInNextCloud('ebR8F8bYAdeHoQB');
  }

  if (/^\/openalt-cz\/2022\/slides(\/)?$/i.test(window.location.pathname)) {
    redirectTo(`https://${nextCloudHostname}/s/d3raWiCkMQQLEgQ`);
  }

  if (/^\/openalt-cz\/2022\/slides\/.+$/i.test(window.location.pathname)) {
    redirectToFileInNextCloud('d3raWiCkMQQLEgQ');
  }

  if (/^\/openalt-cz\/2022\/data\/.+$/i.test(window.location.pathname)) {
    redirectToFileInNextCloud('2RXZzJwYpGe8GRM');
  }

  tryRedirectPhpToHtml();
}

if (window.location.hostname === 'linuxalt.cz' || window.location.hostname === 'www.linuxalt.cz') {
  const linuxAltCzPathInArchive = '/linuxalt-cz';

  if (/^\/$|^.*\.html?$/i.test(window.location.pathname)) { // homepage or HTML file - pass to archive as is
    redirectToInArchive(linuxAltCzPathInArchive, window.location.pathname);
  } else if (/^.+\/$/.test(window.location.pathname)) { // directory - redirect to HTML file in archive
    redirectToInArchive(linuxAltCzPathInArchive, `${window.location.pathname.slice(0, -1)}.html`);
  } else { // everything else - redirect to HTML file in archive
    redirectToInArchive(linuxAltCzPathInArchive, `${window.location.pathname}.html`);
  }
}

if (window.location.hostname === 'lpd.openalt.org') {
  redirectToInArchive('/lpd-openalt-org', window.location.pathname);
}
