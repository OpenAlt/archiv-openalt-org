<!DOCTYPE html>
<html lang="en-US">
  <meta charset="utf-8">
  <title>Redirecting&hellip;</title>
  <link rel="canonical" href="/openalt-cz/2023/media/">
  <script>
    location="/openalt-cz/2023/media/"
  </script>
  <meta http-equiv="refresh" content="0; url=/openalt-cz/2023/media/">
  <meta name="robots" content="noindex">
  <h1>Redirecting&hellip;</h1>
  <a href="/openalt-cz/2023/media/">Click here if you are not redirected.</a>
</html>