
function hide_show_block(name, btn) {
  var which = document.getElementById(name);

  if (which.style.display== 'none') {
    which.style.display="inline";
  } else {
    which.style.display="none";
  }
}

$( document ).ready(function() {
    $('form').bind("keypress", function(e) {
      if (e.keyCode == 13) {
        e.preventDefault();
        return false;
      }
    });
});